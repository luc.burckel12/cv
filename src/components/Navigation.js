import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';


class Navigation extends Component {

    handleClick=()=> {
        let root = document.documentElement;
        if(root.style.getPropertyValue('--color-1')==="#372864"){
            root.style.setProperty('--color-1', "#6973ad");
            root.style.setProperty('--color-2', "#353c8d");
            root.style.setProperty('--color-3', "#439285");
            root.style.setProperty('--color-4', "rgb(89, 125, 167)");
            root.style.setProperty('--black', "rgb(216, 216, 216)");
            root.style.setProperty('--black-1', "rgb(212, 212, 212)");
            root.style.setProperty('--white', "rgb(209, 209, 209)");
            root.style.setProperty('--white-1', "rgb(14, 14, 14)");
            root.style.setProperty('--color-bar', "rgba(220,220,220,0.5)");
            root.style.setProperty('--color-theme', "#60D360");
            root.style.setProperty('--color-sun',"#F8B316");
            root.style.setProperty('--color-moon',"#333");
            root.style.setProperty('--shadow-moon',"none");
            root.style.setProperty('--margin-button',"24px");
        }
        else{
            root.style.setProperty('--color-1', "#372864");
            root.style.setProperty('--color-2', " #242962");
            root.style.setProperty('--color-3', "#2c8576");
            root.style.setProperty('--color-4', "rgb(44, 82, 124)");
            root.style.setProperty('--white', "rgb(187, 187, 187)");
            root.style.setProperty('--black-1', "black");
            root.style.setProperty('--black', "#333");
            root.style.setProperty('--white-1', "white");
            root.style.setProperty('--color-bar', "rgba(180,179,179,0.5)");
            root.style.setProperty('--color-theme', "#333");
            root.style.setProperty('--color-moon',"#000");
            root.style.setProperty('--color-sun',"#333");
            root.style.setProperty('--shadow-moon',"1px 0px 7px white");
            root.style.setProperty('--margin-button',"2px");
        }
      }

    render() {
        return (
            <div className="sidebar">
            <div className="id">
                <div className="idContent">
                    <img src="./media/Luc_Burckel.jpg" alt="profil-pic" height="147" width="150"/>
                    <h3>Luc Burckel</h3>
                </div>
            </div>
            <div className="navigation">
                <ul>
                    <li>
                        <NavLink exact to="" activeClassName="navActive">
                            <i className="fas fa-home"></i>
                            <span>Accueil</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/Formation" activeClassName="navActive">
                            <i className="fas fa-book-open"></i>
                            <span>Formation</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/ExperiencePro" activeClassName="navActive">
                            <i className="fas fa-suitcase"></i>
                            <span>Expériences</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/Skill" activeClassName="navActive">
                            <i className="fas fa-mountain"></i>
                            <span>Connaissances</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/Contact" activeClassName="navActive">
                            <i className="fas fa-address-book"></i>
                            <span>Contact</span>
                        </NavLink>
                    </li>
                </ul>
            </div>

            <div className="social">
                <ul>
                    <li>
                        <a href="https://git.unistra.fr/luc.burckel" target="_blank" rel="nooperner noreferrer"><img src="./media/git.svg" className="gitLab" alt="GitLab"></img></a>
                    </li>
                </ul>
            </div>
            <div className="theme">
            <i class="fas fa-moon" id="moon"></i>
                <button type="button" onClick={this.handleClick}><div id="switch_front"></div></button>
            <i class="fas fa-sun" id="sun"></i>
            </div>
        </div> 
        );
    }
}

export default Navigation;