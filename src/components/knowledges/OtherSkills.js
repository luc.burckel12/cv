import React from 'react';

const OtherSkills = () => {
    return (
        <div className="otherskills">
            <h3>Compétences</h3>
            <div className="list">
                <ul>
                    <li>
                        <i className="fas fa-check-square"></i>
                        <span>Photoshop</span>
                    </li>
                    <li>
                        <i className="fas fa-check-square"></i>
                        <span>Papyrus</span>
                    </li>
                    <li>
                        <i className="fas fa-check-square"></i>
                        <span>Word, Exel, Powerpoint</span>
                    </li>
                </ul>
                <ul>
                    <li>
                        <i className="fas fa-check-square"></i>
                        <span>Linux</span>
                    </li>
                    <li>
                        <i className="fas fa-check-square"></i>
                        <span>Montage PC</span>
                    </li>
                    <li>
                        <i className="fas fa-check-square"></i>
                        <span>Blender, 3dsMax, Cinema4D</span>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default OtherSkills;