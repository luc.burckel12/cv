import React, { Component } from 'react';
import Navigation from '../components/Navigation';
import Ecole from '../components/knowledges/Ecole';

class Formation extends Component {
    state={
        ecole:[
            {id:1, lien:"https://www.fst.uha.fr", annee:"2021 -", ecole:"L3 Informatique", image:"./media/fst.jpg",alt:"Faculté des sciences et techniques"},
            {id:2, lien:"https://iutrs.unistra.fr/", annee:"2019 - 2021", ecole:"DUT Informatique", image:"./media/robertSchuman.jpeg",alt:"IUT Robert Schuman"},
            {id:3, lien:"https://www.hautbarr.net/", annee:"2017 – 2019", ecole:"Bac S - Sciences de l’Ingénieur", image:"./media/hautBarr.jpg",alt:"Lyçée Haut Barr"},
            {id:4, lien:"https://lycee-zeller-bouxwiller.fr/", annee:"2016 – 2017", ecole:"2nd Général", image:"./media/Lycée_de_Bouxwiller.jpg",alt:"Lyçée Adrien Zeller"},
        ],
    }
    render() {
        let {ecole} = this.state;
        return (
            <div className="formation">
                <Navigation/>
                <Ecole
                    ecole={ecole}
                />
            </div>
        );
    }
}

export default Formation;